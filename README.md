# simple-loggerify

> Driver for SimpleLogger Online API

API is live at https://simplelogger.herokuapp.com/

### Install
```
npm install simple-loggerify --save
```
## Usage
### Import
```
const { SimpleLogger } = require('simple-loggerify');
```
### Register
```
SimpleLogger.register({
  apiKey: 'ldTnffK03rrx4JckY6p45KFA8l3vk5sj'
});
```
### Log!
```
SimpleLogger.instance.log('mylog', {a:1});
```
### Error? send urself its log immediately
```
SimpleLogger.instance.error('my error', { msg: "...." });
```

## Get free API Key
> To get an API Key, send email with you company/organization details at `freemaileronline@gmail.com`.  
Details should contain, Company full name, public email address and private email address.