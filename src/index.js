const axios = require('axios');

/**
 * @typedef {{
 * apiKey: string;
 * logOnConsole?: boolean;
 * url?: string;
 * }} SimpleLoggerAttributes
 */

/**
 * @typedef {{
 * id: number;
 * apiKey: string;
 * name: string;
 * level?: number;
 * parentGroupId?: number;
 * }} NestGroup
 */

class SimpleLogger {
  /**
   * Singleton static email service instance
   * @type {SimpleLogger}
   * @static
   */
  static instance = null;

  /**
   * Creates a singleton class  
   * Use `SimpleLogger.register({})` instead of `new SimpleLogger()`
   * @private
   * @param {SimpleLoggerAttributes} attr 
   */
  constructor(attr) {
    if (!SimpleLogger.instance) {
      SimpleLogger.instance = this;
    } else {
      throw new Error('SimpleLogger instance already available, please use `SimpleLogger.instance`');
    }
    this.apiKey = "";
    this.url = "https://simplelogger.herokuapp.com";
    this.logOnConsole = true;

    Object.assign(this, attr);

    /**
     * @type {NestGroup[]}
     */
    this._nestStack = [];
  }

  /**
   * Creates a singleton class.
   * @example
   * SimpleLogger.register({
   *   apiKey: 'ldTnffK03rrx4JckY6p45KFA8l3vk5sj'
   * });
   * @param {SimpleLoggerAttributes} attr 
   * @returns {SimpleLogger} Singleton instance of SimpleLogger
   */
  static register(attr) {
    return new SimpleLogger(attr);
  }

  /**
   * Logs your values into the SimpleLogger.
   * recommended not to `await` for logger to finish logging.
   * @example
   * SimpleLogger.instance.log('hello');
   * SimpleLogger.instance.log('hello', { str:'Welcome!' });
   * @returns Promise of void
   */
  async log() {
    return await this.process('log', arguments);
  }

  /**
   * Logs your values into the SimpleLogger.
   * recommended not to `await` for logger to finish logging.
   * @example
   * SimpleLogger.instance.info('hello');
   * SimpleLogger.instance.info('hello', { str:'Welcome!' });
   * @returns Promise of void
   */
  async info() {
    return await this.process('info', arguments);
  }

  /**
   * Logs your values into the SimpleLogger.
   * recommended not to `await` for logger to finish logging.
   * @example
   * SimpleLogger.instance.warn('something went wrong, but can be recovered!', { str:'Recovering...' });
   * @returns Promise of void
   */
  async warn() {
    return await this.process('warn', arguments);
  }

  /**
   * Logs your values into the SimpleLogger.
   * recommended not to `await` for logger to finish logging.
   * Email will be send to the client notifying about the error.
   * @example
   * SimpleLogger.instance.error('something went wrong', { ex: { message: 'unknown error' } });
   * @returns Promise of void
   */
  async error() {
    return await this.process('error', arguments);
  }

  /**
   * Internal processor
   * @private
   * @param {string} type LogType
   * @param {any[]} args Log data
   * @returns Empty promise
   */
  async process(type, args) {
    let message = "";
    let details = "";
    const topNest = this._nestStack[this._nestStack.length - 1];

    if (args.length == 0) {
      return;
    }

    if (args.length > 2) {
      for (let i = 0; i < args.length - 1; i++) {
        if (typeof args[i] == 'object') {
          message += JSON.stringify(args[i]) + " ";
        } else {
          message += args[i].toString() + " ";
        }
      }
      details = args[args.length - 1];
    } else {
      message = args[0];
      details = args[1];
    }

    if (details == void 0) {
      details = "";
    }

    if (typeof details == 'object') {
      details = JSON.stringify(details);
    }

    if (typeof message == 'object') {
      message = JSON.stringify(message);
    }

    let preStr = '';
    let nestId;
    if (topNest) {
      preStr += "| ".repeat(topNest.level);
      message = preStr + message;
      nestId = topNest.id;
    }

    const data = {
      apiKey: this.apiKey,
      message: message,
      data: details,
      groupId: nestId
    };

    if (this.logOnConsole) {
      console.log(`${new Date().toLocaleString()} ${type}: ${message} ${details}`);
    }
    await axios.post(this.url + "/" + type, data);
  }

  /**
   * Starts nested groups, with given group name
   * @example
   * await SimpleLogger.instance.nestLogs('Calculating');
   * SimpleLogger.instance.log('value', { a: 1 });
   * SimpleLogger.instance.log('updated value', { a: 2 });
   * await SimpleLogger.instance.endNesting();
   * @param {string} name name of the group
   */
  async nestLogs(name) {
    const top = this._nestStack[this._nestStack.length - 1] || { level: 0, id: null };
    const newTop = {
      name,
      apiKey: this.apiKey,
      level: top.level + 1,
      parentGroupId: top.id
    };

    const out = await this.startGroup(newTop.name, newTop.level, newTop.parentGroupId);
    this._nestStack.push(out.data.data);
  }

  /**
   * Ends nesting
   * @returns {Promise<boolean>}
   */
  async endNesting() {
    return await this.endGroup();
  }

  /**
   * Starts grouping of the logs.
   * @private
   * Use `.nestLogs('mygroup')` instead
   * @param {string} name Name of the group
   * @param {number} level Depth of the group
   * @param {number} parentGroupId id of parent
   */
  async startGroup(name, level = 1, parentGroupId = null) {
    const data = {
      apiKey: this.apiKey,
      name,
      level,
      parentGroupId
    };
    this.log('=== STARTING ', name, '===');
    return await axios.post(this.url + "/startGroup", data);
  }

  /**
   * @private
   * Closes grouped logs.
   */
  async endGroup() {
    const top = this._nestStack.pop();
    if (top) {
      top.apiKey = this.apiKey;
      this.log('===  ENDING  ', top.name, '===');
      await axios.post(this.url + "/endGroup", top);
    }
  }
}

/* const a = async () => {
  SimpleLogger.instance.log('anv', 'asd', 'asd');
  await SimpleLogger.instance.nestLogs('mygroup');
  SimpleLogger.instance.log('anv', 'asd', 'asd');
  await SimpleLogger.instance.nestLogs('1.1');
  SimpleLogger.instance.log('asd', { a: 1 });
  SimpleLogger.instance.log({ a: 1 }, 'asds');
  await SimpleLogger.instance.endNesting();
  SimpleLogger.instance.log('anv', 'asd', 'asd', { a: 2 });
  await SimpleLogger.instance.endNesting();
  SimpleLogger.instance.log('anv', 'asd', 'asd');
  await SimpleLogger.instance.nestLogs('g2');
  SimpleLogger.instance.log('anv', 'asd', 'asd');
  SimpleLogger.instance.log('anv', 'asd', 'asd');
  await SimpleLogger.instance.endNesting();
};
SimpleLogger.register({ apiKey: '', url: 'http://localhost:7012' });
a(); */

module.exports = { SimpleLogger };
